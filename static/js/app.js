"use strict";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = _extends({
    tag: 'div',
    type: 'icons',
    "class": '',
    mode: 'inline',
    url: ''
  }, opts);
  var external = '';
  var typeClass = '';

  if (opts.mode === 'external') {
    external = "".concat(opts.url, "/sprite.").concat(opts.type, ".svg");
  }

  if (opts.type !== 'icons') {
    typeClass = " svg-icon--".concat(opts.type);
  }

  opts["class"] = opts["class"] ? " ".concat(opts["class"]) : '';
  return "\n    <".concat(opts.tag, " class=\"svg-icon svg-icon--").concat(name).concat(typeClass).concat(opts["class"], "\" aria-hidden=\"true\" focusable=\"false\">\n      <svg class=\"svg-icon__link\">\n        <use xlink:href=\"").concat(external, "#").concat(name, "\"></use>\n      </svg>\n    </").concat(opts.tag, ">\n  ");
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  };

  if ($('.b-cases__items').length) {
    $('.b-cases__items').owlCarousel({
      items: 1,
      loop: 1,
      nav: 1,
      margin: 10,
      dots: 1,
      smartSpeed: 2000,
      autoplayTimeout: 7000,
      navText: ['<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20.25 11.2502H5.28787L11.0171 5.79318C11.3171 5.50743 11.3287 5.03268 11.043 4.73268C10.7576 4.43306 10.2829 4.42106 9.9825 4.70681L3.4395 10.9393C3.15637 11.2228 3 11.5993 3 12.0002C3 12.4007 3.15637 12.7776 3.45262 13.0734L9.98287 19.2932C10.128 19.4316 10.314 19.5002 10.5 19.5002C10.698 19.5002 10.896 19.4222 11.0434 19.2673C11.3291 18.9673 11.3175 18.4929 11.0175 18.2072L5.26425 12.7502H20.25C20.664 12.7502 21 12.4142 21 12.0002C21 11.5862 20.664 11.2502 20.25 11.2502Z" fill="#8A8A8A"/></svg>', '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M3.75 12.7498L18.7121 12.7498L12.9829 18.2068C12.6829 18.4926 12.6712 18.9673 12.957 19.2673C13.2424 19.5669 13.7171 19.5789 14.0175 19.2932L20.5605 13.0607C20.8436 12.7772 21 12.4007 21 11.9998C21 11.5993 20.8436 11.2224 20.5474 10.9266L14.0171 4.70682C13.872 4.56844 13.686 4.49982 13.5 4.49982C13.302 4.49982 13.104 4.57782 12.9566 4.73269C12.6709 5.03269 12.6825 5.50707 12.9825 5.79282L18.7358 11.2498L3.75 11.2498C3.336 11.2498 3 11.5858 3 11.9998C3 12.4138 3.336 12.7498 3.75 12.7498Z" fill="#8A8A8A"/></svg>']
    });
  }

  if ($('.b-slider__items').length) {
    $('.b-slider__items').owlCarousel({
      items: 1,
      loop: 1,
      nav: 1,
      margin: 10,
      dots: 0,
      smartSpeed: 2000,
      autoplayTimeout: 7000,
      navText: ['<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20.25 11.2502H5.28787L11.0171 5.79318C11.3171 5.50743 11.3287 5.03268 11.043 4.73268C10.7576 4.43306 10.2829 4.42106 9.9825 4.70681L3.4395 10.9393C3.15637 11.2228 3 11.5993 3 12.0002C3 12.4007 3.15637 12.7776 3.45262 13.0734L9.98287 19.2932C10.128 19.4316 10.314 19.5002 10.5 19.5002C10.698 19.5002 10.896 19.4222 11.0434 19.2673C11.3291 18.9673 11.3175 18.4929 11.0175 18.2072L5.26425 12.7502H20.25C20.664 12.7502 21 12.4142 21 12.0002C21 11.5862 20.664 11.2502 20.25 11.2502Z" fill="#8A8A8A"/></svg>', '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M3.75 12.7498L18.7121 12.7498L12.9829 18.2068C12.6829 18.4926 12.6712 18.9673 12.957 19.2673C13.2424 19.5669 13.7171 19.5789 14.0175 19.2932L20.5605 13.0607C20.8436 12.7772 21 12.4007 21 11.9998C21 11.5993 20.8436 11.2224 20.5474 10.9266L14.0171 4.70682C13.872 4.56844 13.686 4.49982 13.5 4.49982C13.302 4.49982 13.104 4.57782 12.9566 4.73269C12.6709 5.03269 12.6825 5.50707 12.9825 5.79282L18.7358 11.2498L3.75 11.2498C3.336 11.2498 3 11.5858 3 11.9998C3 12.4138 3.336 12.7498 3.75 12.7498Z" fill="#8A8A8A"/></svg>'],
      responsive: {
        481: {
          margin: 20,
          items: 2
        },
        769: {
          margin: 20,
          items: 3
        },
        1025: {
          items: 5,
          margin: 30
        }
      }
    });
  }

  if ($('.mySwiper').length) {
    var swiper = new Swiper(".mySwiper", {
      slidesPerView: 1,
      slidesPerColumn: 1,
      spaceBetween: 30,
      pagination: {
        el: ".swiper-pagination",
        clickable: true
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      },
      breakpoints: {
        480: {
          slidesPerView: 2,
          slidesPerColumn: 2,
          spaceBetween: 15
        },
        768: {
          slidesPerView: 3,
          slidesPerColumn: 2,
          spaceBetween: 20
        },
        1024: {
          slidesPerView: 4,
          slidesPerColumn: 2,
          spaceBetween: 30
        }
      }
    });
  }

  var PAGE = $('html, body');
  var pageScroller = $('.page-scroller');
  var inMemoryClass = 'page-scroller--memorized';
  var isVisibleClass = 'page-scroller--visible';
  var enabledOffset = 60;
  var pageYOffset = 0;
  var inMemory = false;

  function resetPageScroller() {
    setTimeout(function () {
      if (window.pageYOffset > enabledOffset) {
        pageScroller.addClass(isVisibleClass);
      } else if (!pageScroller.hasClass(inMemoryClass)) {
        pageScroller.removeClass(isVisibleClass);
      }
    }, 150);

    if (!inMemory) {
      pageYOffset = 0;
      pageScroller.removeClass(inMemoryClass);
    }

    inMemory = false;
  }

  if (pageScroller.length > 0) {
    window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
      passive: true
    } : false);
    pageScroller.on('click', function (event) {
      event.preventDefault();
      window.removeEventListener('scroll', resetPageScroller);

      if (window.pageYOffset > 0 && pageYOffset === 0) {
        inMemory = true;
        pageYOffset = window.pageYOffset;
        pageScroller.addClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: 0
        }, 500, 'swing', function () {
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      } else {
        pageScroller.removeClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: pageYOffset
        }, 500, 'swing', function () {
          pageYOffset = 0;
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      }
    });
  }
});