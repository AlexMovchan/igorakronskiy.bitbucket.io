if ($('.mySwiper').length) {
  var swiper = new Swiper(".mySwiper", {
    slidesPerView: 1,
    slidesPerColumn: 1,
    spaceBetween: 30,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    breakpoints: {
      480: {
        slidesPerView: 2,
        slidesPerColumn: 2,
        spaceBetween: 15,
      },
      768: {
        slidesPerView: 3,
        slidesPerColumn: 2,
        spaceBetween: 20,
      },
      1024: {
        slidesPerView: 4,
        slidesPerColumn: 2,
        spaceBetween: 30,
      }
    }

  });
}
